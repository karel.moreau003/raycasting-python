import pygame, sys
import numpy as np
import main
import time

# General setup
pygame.init()
clock = pygame.time.Clock()

# Setting up the main window
screen_width = 640
screen_height = 480
screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption('RayCasting')

coords_camera = [7, 4.5]
velocity_camera = [0, 0]
direction_camera = [1, 0]

sensitivity = 0.01
speed_factor = .5

start = 0
while True:
    # Handling input
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.KEYDOWN:

            if event.key == pygame.K_d:
                velocity_camera[0] = -direction_camera[1]
                velocity_camera[1] = direction_camera[0]
            if event.key == pygame.K_q:
                velocity_camera[0] = direction_camera[1]
                velocity_camera[1] = - direction_camera[0]
            if event.key == pygame.K_z:
                velocity_camera[0] = direction_camera[0]
                velocity_camera[1] = direction_camera[1]
            if event.key == pygame.K_s:
                velocity_camera[0] = - direction_camera[0]
                velocity_camera[1] = - direction_camera[1]

        if event.type == pygame.KEYUP:
            velocity_camera[0] = 0
            velocity_camera[1] = 0
    # Camera Position
    coords_camera[0] += velocity_camera[0] * speed_factor
    coords_camera[1] += velocity_camera[1] * speed_factor

    # Camera orientation
    mouse_mvt = pygame.mouse.get_rel()[0]
    angle = mouse_mvt * sensitivity
    direction_camera = direction_camera[0] * np.cos(angle) - direction_camera[1] * np.sin(angle), \
                       direction_camera[0] * np.sin(angle) + direction_camera[1] * np.cos(angle)

    # Image creation and printing
    image = np.array(main.create_image(direction_camera, coords_camera))
    image = np.transpose(image, (1, 0, 2))
    pygame.surfarray.blit_array(screen, image)
    pygame.display.flip()
    clock.tick(60)
