from math import sin, cos, pi
import time


def mesurer_temps(f):
    def wrapper(*args, **kargs):
        start = time.time()
        resultat = f(*args, **kargs)
        print(f"la fonction : {f.__name__} a pris : {time.time() - start}")
        return resultat

    return wrapper


def rotation_vec(v, a):
    return [v[0] * cos(a) - v[1] * sin(a), v[0] * sin(a) + v[1] * cos(a)]


def get_ray_intersection(u_ray, coords_camera):
    count = [0, 0]
    nextx = int(coords_camera[0]) if u_ray[0] < 0 else int(coords_camera[0]) + 1
    nexty = int(coords_camera[1]) if u_ray[1] < 0 else int(coords_camera[1]) + 1
    if u_ray[0] == 0:
        u_ray[0] = 10 ** 100
    if u_ray[1] == 0:
        u_ray[1] = 10 ** 100
    dtx, dty = (nextx - coords_camera[0]) / u_ray[0], (nexty - coords_camera[1]) / u_ray[1]
    mapx, mapy = int(coords_camera[0]), int(coords_camera[1])
    while coords_camera[0] + sum(count) * u_ray[0] not in [0, MAP_SIZE[0] - 1] and coords_camera[1] + sum(count) * u_ray[1] not in [0,
                                                                                                                  MAP_SIZE[
                                                                                                                      1] - 1]:
        if dtx < dty:
            count[0] += dtx
            dty -= dtx
            dtx = abs(1 / u_ray[0])
            mapx += abs(u_ray[0]) / u_ray[0]

        else:
            count[1] += dty
            dtx -= dty
            dty = abs(1 / u_ray[1])
            mapy += abs(u_ray[1]) / u_ray[1]
        yield [round(coords_camera[0] + sum(count) * u_ray[0], 2), round(coords_camera[1] + sum(count) * u_ray[1], 2)], [int(mapx),
                                                                                                       int(mapy)]


def create_row(distance, case, brightness):
    wall_height = int(SCREEN_HEIGHT / distance)
    if case == 1:
        color = (255 * brightness, 0, 0)

    if case == 2:
        color = (255 * brightness, 255 * brightness, 0)

    row = floor[:int((SCREEN_HEIGHT - wall_height) / 2)]
    row.extend([color for _ in range(wall_height)])
    row.extend(sky[:SCREEN_HEIGHT - len(row)])
    # row = [(0, 102, 255) for _ in range(int((SCREEN_HEIGHT - wall_height) / 2))]
    # row.extend([color for _ in range(wall_height)])
    # row.extend((102, 102, 153) for _ in range(SCREEN_HEIGHT - len(row)))
    return row


def handle_a_ray(u_ray, coords_camera, angle):
    gen_inter = get_ray_intersection(u_ray, coords_camera)
    co_inter, co_map = next(gen_inter)
    while world_map[co_map[0]][co_map[1]] == 0:
        co_inter, co_map = next(gen_inter)
    distance = ((coords_camera[0] - co_inter[0]) ** 2 + (coords_camera[1] - co_inter[1]) ** 2) ** 0.5 * cos(angle)
    if co_inter[0] == int(co_inter[0]):
        brightness = 0.8
    else:
        brightness = 1
    return create_row(distance, world_map[co_map[0]][co_map[1]], brightness)


def create_image(u_p, coords_camera):
    image_T = []
    i = -FOV / 2
    step = FOV / SCREEN_WIDTH
    # print(rotation_vec(u_j, i*pi/180))
    while i < FOV / 2:
        ray_direction = rotation_vec(u_p, i * pi / 180)
        image_T.append(handle_a_ray(ray_direction, coords_camera, i * pi / 180))
        i += step
    # print(ray_direction)
    return list(zip(*image_T))


world_map = ((1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
             (1, 0, 0, 0, 0, 0, 0, 0, 2, 1),
             (1, 0, 0, 0, 0, 0, 0, 0, 0, 1),
             (1, 0, 0, 0, 0, 0, 0, 0, 0, 1),
             (1, 0, 0, 0, 2, 2, 0, 0, 0, 1),
             (1, 0, 0, 0, 2, 2, 0, 0, 0, 1),
             (1, 0, 0, 0, 0, 0, 0, 0, 0, 1),
             (1, 0, 0, 0, 0, 0, 0, 0, 0, 1),
             (1, 0, 0, 0, 0, 0, 0, 0, 0, 1),
             (1, 1, 1, 1, 1, 1, 1, 1, 1, 1))

MAP_SIZE = (len(world_map[0]), len(world_map))
SCREEN_HEIGHT = 480
SCREEN_WIDTH = 640
FOV = 90
sky = [(102, 102, 153) for _ in range(SCREEN_HEIGHT)]
floor = [(0, 102, 255) for _ in range(SCREEN_HEIGHT)]
