# Ray Casting Python

## Overview

Ray Casting Python is a 3D engine that employs the ray casting technique. It's a pure mathematical and algorithmic exploration. 

![Project Screenshot](image.png)


## Features

- 3D Ray Casting Engine
- Map customization with red and yellow walls (1 and 2)
- Camera movement using 'z', 'q', 's', and 'd' keys

## Installation and Usage

1. **Clone Repository**: Clone this repository to your local machine.
2. **Dependencies**: Ensure you have Python 3.10 installed and install required libraries using `pip install pygame numpy`.
3. **Run the Project**: Execute the `UI.py` file to launch the application.
4. **Customize the Map**: Modify the map layout in the `main.py` file.
5. **Camera Control**: Move the camera using 'z', 'q', 's', and 'd' keys.
